var gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    watch = require('gulp-watch'),
    gulpWebpack = require('gulp-webpack'),
    webpack = require('webpack'),
    replace = require('gulp-replace'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass');

gulp.task('webserver', function() {
  gulp.src('./').pipe(webserver({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

gulp.task('sass', function () {
  return gulp.src('./src/scss/style.scss')
    .pipe(sass({
    	outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('./dist'));
});

/*

// Copy vednor files from node_modules to dist/vendor
gulp.task('vendor', function() {
   return gulp.src([
    'node_modules/some/module.js' ,
  ])
  .pipe( gulp.dest('./dist/vendor') );
});

*/

// Webpack JS bundle
gulp.task('webpack', function() {
  gulp.src('./src/js/index.js')
    .pipe(gulpWebpack({
      output: {
        filename: 'bundle.min.js',
      },
      // devtool: "source-map",
      plugins: [
        new webpack.optimize.UglifyJsPlugin({
          minimize: true
        }),
        new webpack.optimize.DedupePlugin()
      ]
    }))
    .pipe(gulp.dest('./dist'));
});

// Compilation timestamp at the end of process
gulp.task('version', [ 'webpack' , 'sass'] , function() {
    return gulp.src(['index.html'])
    .pipe(replace( /\?v=(.*)\"/g, '?v='+ new Date().getTime() + '\"'))
    .pipe( gulp.dest('./') );
});

gulp.task('watch', function() {
  gulp.watch(
    ['src/**/*.*'] , [ 'webpack' , 'sass' , 'version' ]
  );
});

gulp.task( 'default' , [ 'webpack' , 'sass' , 'version' ] );

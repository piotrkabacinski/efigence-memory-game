# Memory

## Instalacja 

    $ git clone --depth=1 https://bitbucket.org/piotrkabacinski/efigence-memory-game
    $ cd Efigence-Memory-Game
    $ npm install
    $ gulp
    $ gulp webserver # http://localhost:8000/index.html

## Działanie

Dane pobierane są z pliku `cards.json`. Zawiera on 2 właściwości: tablicę z linkami do obrazków oraz ilość kart w rzędzie (domyślnie 3).

Logikę biznesową najlepiej odzwierciedalają funkcje użyte w metodzie `init` po sukcesywnym pobraniu JSON-a:

    
    // src/js/memory.js:~22

    context.save( data )
		   .createGroups()
		   .createPairs()
		   .shuffleGroups()
		   .printCards();

## Kompatybilność

 1. IE 11
 2. Opera 38
 3. Safar 9.1
 4. FF 47
 5. Chrome 51
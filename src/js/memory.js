var $ = require("jQuery");

function Memory() {

	"use strict";

	this.cards = [];
	this.groups = [];
	this.selectedGroup = null;
	this.triesAmount = 0;
	this.pairsFound = 0;
	this.cardSize = "33%"; // default value

}

Memory.prototype.init = function( jsonPath ) {

	"use strict";

	var path = jsonPath || "cards.json",
		context = this;

	$.getJSON( path , function( data ) {

		context.save( data )
				   .createGroups()
				   .createPairs()
				   .shuffleGroups()
				   .printCards();

	});

};

Memory.prototype.save = function( data ) {

	"use strict";

	this.cards = data.cards;
	this.cardSize = Math.round( (100 / data.cols) * 100) / 100;

	return this;

};

Memory.prototype.createGroups = function() {

	"use strict";

	var context = this;

	this.cards.map(function( value, index ) {

		context.groups.push({
			group: index,
			image: value
		});

	});

	return this;

};

Memory.prototype.createPairs = function() {

	"use strict";

	var l = this.groups.length,
		i = 0;

	for( i; i < l; i++ ) {

		this.groups.push({
			group: this.groups[i].group,
			image: this.groups[i].image
		});

	}

	console.log( this.groups );

	return this;
};

// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
Memory.prototype.shuffleGroups = function() {

	"use strict";

	var counter = this.groups.length;

    while (counter > 0) {

        var index = Math.floor( Math.random() * counter );

        counter--;

        var temp = this.groups[counter];
        this.groups[counter] = this.groups[index];
        this.groups[index] = temp;
    }

    return this;

};

Memory.prototype.printCards = function() {

	"use strict";

	var context = this,
		l = this.groups.length,
		i = 0;

	for( i; i < l; i++ ) {

		$("#memory").append(
			"<div class='card unpaired' data-group='"+this.groups[i].group+"' style='background-image:url("+this.groups[i].image+"); width: calc( "+this.cardSize+"% - 5px ); padding-top: "+this.cardSize+"%;'></div>"
		);

	}

	$(".unpaired").on("click", function( e ) {

		context.eventsHandler( $(this) );

	});

};

Memory.prototype.eventsHandler = function( element ) {

	"use strict";

	if( element ) {

		// Save selection
		if( !this.selectedGroup ) {

			if( !element.hasClass("found") && !element.hasClass("blocked")  ) {

				this.selectedGroup = element.attr("data-group");
				element.addClass("selected").removeClass("unpaired");

			}

		// Compare choice with previous one
		} else if( !element.hasClass("selected") ) {

			this.triesAmount += 1;

			element.addClass("selected").removeClass("unpaired");

			// Selections belongs to the same group
			if( this.selectedGroup === element.attr("data-group") ) {

				$(".selected").removeClass("unpaired selected")
							  			.addClass("found");

				this.pairsFound += 1;

				if( this.pairsFound === this.cards.length ) {

					alert("Gratulacje! "+this.pairsFound+" pary w "+this.triesAmount+" prób! :)");

				}

			} else {

				$(".card").addClass("blocked");

				setTimeout(function() {

					$(".selected").removeClass("selected").addClass("unpaired");
					$(".card").removeClass("blocked");

				}, 1000);

			}

			this.selectedGroup = null;

		}

	} else {

		console.error("No jQuery element object");

	}

};

module.exports = Memory;
